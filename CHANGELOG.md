# Change Log

## 0.1.0

### Minor Changes

- retain cursor on editor when the swimlane is already previewed
- 3557b5c: move preview into sidebar

All notable changes to the "swimlanes" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

- Initial release
