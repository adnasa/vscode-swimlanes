import { Webview, Uri } from "vscode";

function createWebView(_webview: Webview, content: string, _extensionUri: Uri) {
  return `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Swimlanes preview</title>
    </head>
    <body class="dark h-96" style="min-height: 100%; display: block;">
      <div style="min-height: 1000px;">
        <swimlanes-io>
        ${content}
        </swimlanes-io>
      </div>
      <script src="https://cdn.swimlanes.io/embed.js"></script>
    </body>
    </html>
  `;
}

export default createWebView;
