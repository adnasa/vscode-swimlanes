import * as vscode from "vscode";
import * as fs from "fs";
import * as path from "path";
import createWebView from "./create-web-view";

const supportedExtensions = [".sl"];

const commandName = "swimlanes.preview";

const panelInstances: {
  [key: string]: vscode.WebviewPanel;
} = {};

const openSwimlane = (context: vscode.ExtensionContext, openFile: string) => {
  const content = fs.readFileSync(openFile, "utf-8");

  const webViewPanel =
    panelInstances[openFile] ||
    vscode.window.createWebviewPanel(commandName, "", vscode.ViewColumn.Two, {
      enableScripts: true,
      retainContextWhenHidden: true,
      localResourceRoots: [vscode.Uri.joinPath(context.extensionUri, "media")],
    });

  webViewPanel.webview.html = createWebView(
    webViewPanel.webview,
    content,
    context.extensionUri
  );

  panelInstances[openFile] = webViewPanel;

  webViewPanel.onDidDispose(() => {
    delete panelInstances[openFile];
  });
};

export function activate(context: vscode.ExtensionContext) {
  const disposable = vscode.commands.registerCommand(commandName, async () => {
    var openFilePath = vscode?.window?.activeTextEditor?.document.uri.fsPath;

    if (openFilePath) {
      const fileExtension = path.extname(openFilePath);
      if (supportedExtensions.includes(fileExtension)) {
        openSwimlane(context, openFilePath);
      }
      vscode.window.showInformationMessage("Not a swimlane");
    }

    return;
  });

  vscode.workspace.onDidSaveTextDocument((document) => {
    if (panelInstances[document.uri.fsPath] !== undefined) {
      openSwimlane(context, document.uri.fsPath);
    }
  });

  context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}
